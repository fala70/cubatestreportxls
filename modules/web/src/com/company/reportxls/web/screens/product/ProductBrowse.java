package com.company.reportxls.web.screens.product;

import com.haulmont.cuba.gui.screen.*;
import com.company.reportxls.entity.Product;

@UiController("reportxls_Product.browse")
@UiDescriptor("product-browse.xml")
@LookupComponent("productsTable")
@LoadDataBeforeShow
public class ProductBrowse extends StandardLookup<Product> {
}