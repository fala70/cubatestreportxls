package com.company.reportxls.web.screens.product;

import com.haulmont.cuba.gui.screen.*;
import com.company.reportxls.entity.Product;

@UiController("reportxls_Product.edit")
@UiDescriptor("product-edit.xml")
@EditedEntityContainer("productDc")
@LoadDataBeforeShow
public class ProductEdit extends StandardEditor<Product> {
}