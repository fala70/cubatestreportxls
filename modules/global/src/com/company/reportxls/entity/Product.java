package com.company.reportxls.entity;

import com.haulmont.chile.core.annotations.NamePattern;
import com.haulmont.cuba.core.entity.StandardEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@NamePattern("%s|name")
@Table(name = "REPORTXLS_PRODUCT")
@Entity(name = "reportxls_Product")
public class Product extends StandardEntity {
    private static final long serialVersionUID = -1839548008942301867L;

    @Column(name = "NAME")
    protected String name;

    @Column(name = "WEIGHT")
    protected Double weight;

    @Column(name = "SIZE_X")
    protected Integer sizeX;

    @Column(name = "SIZE_Y")
    protected Integer sizeY;

    public Integer getSizeY() {
        return sizeY;
    }

    public void setSizeY(Integer sizeY) {
        this.sizeY = sizeY;
    }

    public Integer getSizeX() {
        return sizeX;
    }

    public void setSizeX(Integer sizeX) {
        this.sizeX = sizeX;
    }

    public Double getWeight() {
        return weight;
    }

    public void setWeight(Double weight) {
        this.weight = weight;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}